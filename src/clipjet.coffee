window.Clipjet = class Clipjet
  @url = "http://192.168.0.15:3000/api"
  constructor: ->
    @success = true
    @width = @readWidthFromDiv()
    @height = @readHeightFromDiv()
    @category = @readCategoryFromDiv()
    @site_token = @readSiteTokenFromDiv()
    @language = @readLanguageFromDiv()
    @started = false
    @ended = false

  initFrame: ->
    @iframe = document.createElement('div')
    @setIframeAttributes(@width, @height)
    document.getElementById('clipjet-video').appendChild(@iframe)

  init: ->
    @initFrame()
    @initPlayer()

  setIframeAttributes: (width = @width, height = @height) ->
    @iframe.setAttribute('id',"player")#; @iframe.height = height; @iframe.width = width
    # @iframe.setAttribute('type', "type/html"); @iframe.setAttribute('frameborder', '0'); 


  setCampaignToken: (token) ->
    @campaign_token = token

  setToken: (token) ->
    @token = token

  setMinElapsedTime: () =>
    if @player.getDuration() > 100 then @minElapsedTime = 30 else @minElapsedTime = 0.3*@player.getDuration()

  instantiateSocialTab: () ->
    @socialTab = new SocialTab(@getYtURLFromId(), @width)

  insertSocialTab: () ->
    @instantiateSocialTab()
    @socialTab.insert()

  getYTUrl: (url) ->
    "http://www.youtube.com/embed/#{Clipjet.parseYTUrl(url)}?enablejsapi=1&origin=#{Clipjet.getOrigin()}"

  getYtURLFromId: () ->
    "http://www.youtube.com/watch?v=#{@videoId}"

  xss_ajax: (url) ->
    script = document.createElement('script')
    script.setAttribute('type', 'text/javascript')
    script.setAttribute('src', url)
    script.setAttribute('id', 'clipjet_xss_script_id')
    script_id = document.getElementById('clipjet_xss_script_id') || null

    if script_id
        script_id.parentNode.removeChild(script_id)

    # Insert <script> into DOM
    document.getElementsByTagName('head')[0].appendChild(script)

  initPlayer: ->
    @xss_ajax("#{Clipjet.url}/videos/show.js?category_id=#{@category}&site_token=#{@site_token}&language=#{@language}")

  insertVideo: (data) ->
    @iframe.src = @getYTUrl(data.video_url)
    @videoId = Clipjet.parseYTUrl(data.video_url)
    @setCampaignToken(data.campaign_token)
    @initYTPlayer()

  readLanguageFromDiv: ->
    document.getElementById('clipjet-video').getAttribute('data-lang')    

  readSiteTokenFromDiv: ->
    document.getElementById('clipjet-video').getAttribute('data-site-token')

  readWidthFromDiv: ->
    document.getElementById('clipjet-video').getAttribute('width')

  readHeightFromDiv: ->
    document.getElementById('clipjet-video').getAttribute('height')

  readCategoryFromDiv: ->
    parseInt document.getElementById('clipjet-video').getAttribute('data-category')

  initYTPlayer: ->
    s = document.createElement("script")
    s.src = "#{if location.protocol == 'https:' then 'https' else 'http'}://www.youtube.com/iframe_api"
    before = document.getElementsByTagName("script")[0]
    before.parentNode.insertBefore(s, before)

  checkVideoStatus: (event) =>
    switch event.data
      when 1 #YT.PlayerState.PLAYING 
        unless @started
          @started = true
          @notifyVideoStarted()
          # CREATE METHOD TO UPDATE STATUS IF ELAPSED TIME > minElapsedTime
          @setVideoUpdateInterval()
      when 0
        unless @ended
          @ended = true
          @notifyVideoUpdate(1)

  checkVideoTime: =>
    @setMinElapsedTime unless @minElapsedTime > 0
    if @player.getPlayerState() == 1 and @minElapsedTime > 0 and @player.getCurrentTime() > @minElapsedTime
      @notifyVideoUpdate()
      clearInterval(@intervalID)

  sendUpdateRequest: (url) ->
    img = document.createElement('img')
    body = document.getElementsByTagName('body')
    img.setAttribute('class','update-clipjet')
    img.setAttribute('style','visibility:hidden;');
    img.setAttribute('style','display:none;');
    img.src = url
    body[0].appendChild(img);

  onYTReady: () =>
    @setMinElapsedTime()
    @insertSocialTab()

  setVideoUpdateInterval: ->
    @intervalID = setInterval(@checkVideoTime, 500)

  notifyVideoUpdate: (endOfVideo = 0) ->
    if @success
      json = {hit : {elapsed_time : @player.getCurrentTime(), end_of_video : endOfVideo}}
      url = Clipjet.getVideoUpdateUrl(@token, json)
      @sendUpdateRequest(url)

  notifyVideoStarted: ->
    json = {hit : { site_token : @site_token, article_url : Clipjet.getPathname(), campaign_token : @campaign_token }}
    url = Clipjet.getVideoStartedUrl(json) 
    @xss_ajax(url)

  @parseYTUrl: (yt_url) ->
    yt_url.replace(/^(http|https):\/\/www\.youtube\.com\/watch\?v=/, "")

  @makeUrlFromNestedJson: (json) ->
    result = ""
    for nested, nest of json
      for k,v of nest
        result += "#{encodeURIComponent(nested)}[#{encodeURIComponent(k)}]=#{v}&"
    result.slice(0, -1)

  @getOrigin: ->
    window.location.origin

  @getPathname: ->
    window.location.pathname

  @getVideoStartedUrl: (json) ->
    "#{Clipjet.url}/hit/create?#{Clipjet.makeUrlFromNestedJson(json)}"

  @getVideoUpdateUrl: (token, json) ->
    "#{Clipjet.url}/hit/update?token=#{token}&#{@makeUrlFromNestedJson(json)}"


window.SocialTab = class SocialTab
  constructor: (url, width='200px') ->
    @url = url
    @width = width

  insertDiv: () ->
    @div = document.createElement('div')
    @div.setAttribute('class', 'clipjet-socialTab')
    @div.style.width = @width
    @div.style.height = '29px'
    @div.style.backgroundColor = '#000'
    @div.style.margin = '-4px 0px 0px'
    @div.style.padding = 0
    @div.style.border = 'none'
    document.getElementById('clipjet-video').appendChild(@div)

  insertClipjetLogo: () ->
    logoAnchor = document.createElement('a')
    logoAnchor.setAttribute('class', 'clipjet-logo')
    logoAnchor.style.padding = '8px 0 0'
    logoAnchor.style.display = 'inline-block'
    logoAnchor.style.margin = '0px 0px 0px 8px'
    logoAnchor.href = 'http://www.clipjet.me'
    img = document.createElement('img')
    img.src = 'http://widget.clipjet.me/logo/clipjet.png'
    img.alt = 'ClipJet Link'
    img.style.padding = 0
    img.style.margin = 0
    img.style.verticalAlign = 'bottom'
    img.style.border = 'none'
    logoAnchor.appendChild(img)
    @div.appendChild(logoAnchor)

  setFBShare: () ->
    @fb = document.createElement('a')
    @fb.setAttribute('class', 'clipjet-fb')
    @fb.style.float = 'right'
    @fb.style.styleFloat = 'right'
    @fb.style.cssFloat = 'right'
    @fb.style.margin = '2px'
    @fb.style.padding = 0
    @fb.style.border = 'none'
    @fb.target = '_blank'
    @fb.href = "https://www.facebook.com/sharer/sharer.php?u=#{encodeURIComponent(@url)}"
    @fb.setAttribute('onclick', "window.open('https://www.facebook.com/sharer/sharer.php?u=#{encodeURIComponent(@url)}','facebook-share-dialog','width=626,height=436');return false;")
    @setFBImage()

  setFBImage: () ->
    img = document.createElement('img')
    img.src = 'http://widget.clipjet.me/social/fb.png'
    img.alt = 'Facebook Share Link'
    img.style.border = 'none'
    @fb.appendChild(img)    

  insertFBShare: () ->
    @setFBShare()
    @div.appendChild(@fb)

  insertTwitterShare: () ->
    @setTwitterShare()
    @div.appendChild(@twitter)

  setTwitterShare: () ->
    @twitter = document.createElement('a')
    @twitter.setAttribute('class', 'clipjet-twitter')
    @twitter.style.float = 'right'
    @twitter.style.styleFloat = 'right';
    @twitter.style.cssFloat = 'right';
    @twitter.style.margin = '2px'
    @twitter.style.padding = 0
    @twitter.style.border = 'none'
    @twitter.target = '_blank'
    @twitter.href = "https://twitter.com/share?via=clipjet&url=#{encodeURIComponent(@url)}"
    @twitter.setAttribute('onclick', "window.open('https://twitter.com/share?via=clipjet&url=#{encodeURIComponent(@url)}','twitter-share-dialog','width=626,height=436');return false;")
    @setTwitterImage()

  setTwitterImage: () ->
    img = document.createElement('img')
    img.src = 'http://widget.clipjet.me/social/twitter.png'
    img.alt = 'Twitter Share Link'
    img.style.border = 'none'
    @twitter.appendChild(img)


# <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://colomer.jp" data-text="Check this Video" data-via="clipjet" data-count="none">Tweet</a>
# <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

  insert: () ->
    @insertDiv()
    @insertClipjetLogo()
    @insertFBShare()
    @insertTwitterShare()

window.onYouTubeIframeAPIReady = ->
  clipjet.player = new YT.Player('player',
      width: clipjet.width
      height: clipjet.height
      videoId: clipjet.videoId
      events:
        onStateChange: clipjet.checkVideoStatus
        onReady: clipjet.onYTReady
        # onReady: (e) -> 
        #   console.log 'ready'
    )

initPlayer = ->
  window.clipjet = new Clipjet()
  clipjet.init()

window.onload = ->
  initPlayer()