describe 'SocialTab', ->
  beforeEach ->
    fixture = '''
      <div id="clipjet-video" data-category="2" width="200px" height="200px" data-site-token="12345" data-lang="en">
      </div>
    '''
    setFixtures(fixture)


  describe '.new', ->
    it "sets url to url1", ->
      socialTab = new SocialTab('url1')
      expect(socialTab.url).toBe 'url1'
    it "sets url to url2", ->
      socialTab = new SocialTab('url2')
      expect(socialTab.url).toBe 'url2'
 
  describe '#insert', ->
    socialTab = new SocialTab('http://www.youtube.com/watch?v=ASRn1c_Jj24')
    beforeEach ->
      spyOn(socialTab, 'insertDiv')
      spyOn(socialTab, 'insertClipjetLogo')
      spyOn(socialTab, 'insertFBShare')
      spyOn(socialTab, 'insertTwitterShare')

    it "calls insertDiv", ->
      socialTab.insert()
      expect(socialTab.insertDiv).toHaveBeenCalled()    

    it "calls insertClipjetLogo", ->
      socialTab.insert()
      expect(socialTab.insertClipjetLogo).toHaveBeenCalled()

    it "calls insertFBShare", ->
      socialTab.insert()
      expect(socialTab.insertFBShare).toHaveBeenCalled()

    it "calls insertTwitterShare", ->
      socialTab.insert()
      expect(socialTab.insertTwitterShare).toHaveBeenCalled()

  describe '#insertDiv', ->
    it "inserts bottom div", ->
      socialTab = new SocialTab('bla')
      socialTab.insertDiv()
      expect($('#clipjet-video .clipjet-socialTab')).toHaveCss({height: '29px'})
      expect($('#clipjet-video .clipjet-socialTab')).toHaveCss({width: '200px'})
      expect($('#clipjet-video .clipjet-socialTab')).toHaveCss({margin: '-4px 0px 0px'})
      expect($('#clipjet-video .clipjet-socialTab')).toHaveCss({padding: '0px'})
      expect($('#clipjet-video .clipjet-socialTab')).toHaveCss({padding: '0px'})

  describe "#insertClipjetLogo", ->
    it "inserts anchor with image of Clipjet", ->
      socialTab = new SocialTab('bla')
      socialTab.insertDiv()
      socialTab.insertClipjetLogo()
      expect($('#clipjet-video .clipjet-socialTab .clipjet-logo')).toHaveCss({margin: '0px 0px 0px 8px'})
      expect($('#clipjet-video .clipjet-socialTab .clipjet-logo')).toHaveCss({padding: '8px 0px 0px'})
      expect($('#clipjet-video .clipjet-socialTab .clipjet-logo')).toHaveAttr('href', 'http://www.clipjet.me')
      expect($('#clipjet-video .clipjet-socialTab .clipjet-logo img')).toHaveCss({padding: '0px'})
      expect($('#clipjet-video .clipjet-socialTab .clipjet-logo img')).toHaveCss({margin: '0px'})
      expect($('#clipjet-video .clipjet-socialTab .clipjet-logo img')).toHaveCss({'vertical-align': 'bottom'})
      expect($('#clipjet-video .clipjet-socialTab .clipjet-logo img')).toHaveAttr('src', 'http://widget.clipjet.me/logo/clipjet.png')

  describe '#insertFBShare', ->
    socialTab = new SocialTab('http://www.youtube.com/watch?v=ASRn1c_Jj24')
    beforeEach ->
      socialTab.insertDiv()
      spyOn(socialTab, 'setFBShare').andCallThrough()
      spyOn(socialTab, 'setFBImage')

    it "calls setFBShare", ->
      socialTab.insertFBShare()
      expect(socialTab.setFBShare).toHaveBeenCalled()

    it "calls setFBImage", ->
      socialTab.insertFBShare()
      expect(socialTab.setFBImage).toHaveBeenCalled()

    it "inserts FB Link", ->
      socialTab.insertDiv()
      socialTab.insertFBShare()
      expect($('#clipjet-video .clipjet-socialTab')).toContain('.clipjet-fb')

  describe '#setFBShare', ->
    socialTab = new SocialTab('http://www.youtube.com/watch?v=ASRn1c_Jj24')
    beforeEach ->
      socialTab.insertDiv()
      socialTab.setFBShare()
      spyOn(socialTab, 'setFBImage')

    it "sets margin 0", ->
      expect(socialTab.fb).toHaveCss(margin: '2px')

    it "sets padding 0", ->
      expect(socialTab.fb).toHaveCss(padding: '0px')

    it "sets float to right", ->
      expect(socialTab.fb).toHaveCss(float: 'right')

    it "sets float to right", ->
      expect(socialTab.fb).toHaveCss(border: 'none')

    it "sets class to clipjet-fb", ->
      expect(socialTab.fb.getAttribute('class')).toEqual('clipjet-fb')

    it 'sets href to #', ->
      expect(socialTab.fb.getAttribute('href')).toEqual('#')

    it 'sets onclick to https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DASRn1c_Jj24', ->
      expect(socialTab.fb.getAttribute('onclick')).toContain('https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DASRn1c_Jj24')

    it 'sets onclick to https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DCOkudtssUGs', ->
      socialTab.url = 'http://www.youtube.com/watch?v=COkudtssUGs'
      socialTab.setFBShare()
      expect(socialTab.fb.getAttribute('onclick')).toContain('https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DCOkudtssUGs')

  describe '#setFBImage', ->
    socialTab = new SocialTab('http://www.youtube.com/watch?v=ASRn1c_Jj24')
    beforeEach ->
      socialTab.insertDiv()
      socialTab.insertFBShare()

    it "appends image to fb", ->
      expect(socialTab.fb).toContain('img')
      expect($('.clipjet-fb img')).toHaveAttr('src', 'http://widget.clipjet.me/social/fb.png')


  describe '#insertTwitterShare', ->
    socialTab = new SocialTab('http://www.youtube.com/watch?v=ASRn1c_Jj24')
    beforeEach ->
      socialTab.insertDiv()
      spyOn(socialTab, 'setTwitterShare').andCallThrough()
      spyOn(socialTab, 'setTwitterImage')

    it "calls setTwitterShare", ->
      socialTab.insertTwitterShare()
      expect(socialTab.setTwitterShare).toHaveBeenCalled()

    it "calls setTwitterImage", ->
      socialTab.insertTwitterShare()
      expect(socialTab.setTwitterImage).toHaveBeenCalled()

    it "inserts Twitter Link", ->
      socialTab.insertDiv()
      socialTab.insertTwitterShare()
      expect($('#clipjet-video .clipjet-socialTab')).toContain('.clipjet-twitter')


  describe '#setTwitterShare', ->
    socialTab = new SocialTab('http://www.youtube.com/watch?v=ASRn1c_Jj24')
    beforeEach ->
      socialTab.insertDiv()
      socialTab.setTwitterShare()
      spyOn(socialTab, 'setTwitterImage')

    it "sets margin 0", ->
      expect(socialTab.twitter).toHaveCss(margin: '2px')

    it "sets padding 0", ->
      expect(socialTab.twitter).toHaveCss(padding: '0px')

    it "sets float to right", ->
      expect(socialTab.twitter).toHaveCss(float: 'right')

    it "sets float to right", ->
      expect(socialTab.twitter).toHaveCss(border: 'none')

    it "sets class to clipjet-twitter", ->
      expect(socialTab.twitter.getAttribute('class')).toEqual('clipjet-twitter')

    it 'sets href to #', ->
      expect(socialTab.twitter.getAttribute('href')).toEqual('#')

    it 'sets onclick to https://twitter.com/share?via=clipjet&url=http%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DASRn1c_Jj24', ->
      expect(socialTab.twitter.getAttribute('onclick')).toContain('https://twitter.com/share?via=clipjet&url=http%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DASRn1c_Jj24')

    it 'sets onclick to https://twitter.com/share?via=clipjet&url=http%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DCOkudtssUGs', ->
      socialTab.url = 'http://www.youtube.com/watch?v=COkudtssUGs'
      socialTab.setTwitterShare()
      expect(socialTab.twitter.getAttribute('onclick')).toContain('https://twitter.com/share?via=clipjet&url=http%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DCOkudtssUGs')

  describe '#setTwitterImage', ->
    socialTab = new SocialTab('http://www.youtube.com/watch?v=ASRn1c_Jj24')
    beforeEach ->
      socialTab.insertDiv()
      socialTab.insertTwitterShare()

    it "appends image to twitter", ->
      expect(socialTab.twitter).toContain('img')
      expect($('.clipjet-twitter img')).toHaveAttr('src', 'http://widget.clipjet.me/social/twitter.png')
